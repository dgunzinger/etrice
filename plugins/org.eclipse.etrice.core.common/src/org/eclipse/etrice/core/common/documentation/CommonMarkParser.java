package org.eclipse.etrice.core.common.documentation;

import java.util.Arrays;
import java.util.List;

import org.commonmark.Extension;
import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.ext.task.list.items.TaskListItemsExtension;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

public class CommonMarkParser {

	/**
	 * Parses <a href="https://commonmark.org/">CommonMark</a> with some Github extensions like tables.<br>
	 * See parser details <a href="https://github.com/commonmark/commonmark-java">commonmark-java</a>.
	 * @param text
	 * @return html
	 */
	public static String toHtml(String text) {
		List<Extension> extensions = Arrays.asList(TablesExtension.create(), StrikethroughExtension.create(),
				TaskListItemsExtension.create());
		Parser parser = Parser.builder().extensions(extensions).build();
		HtmlRenderer renderer = HtmlRenderer.builder().extensions(extensions).build();
		return renderer.render(parser.parse(text));
	}

}
