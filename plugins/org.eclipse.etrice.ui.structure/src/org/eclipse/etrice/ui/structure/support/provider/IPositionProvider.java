/*******************************************************************************
 * Copyright (c) 2012 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug
 * 
 *******************************************************************************/

package org.eclipse.etrice.ui.structure.support.provider;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.ui.structure.support.Pos;
import org.eclipse.etrice.ui.structure.support.PosAndSize;

public interface IPositionProvider {
	/**
	 * Returns a new position provider.
	 * parent determines align and scaling
	 * 
	 * @param parent
	 * @param invisibleRect
	 * @param innerRect
	 * @return
	 */
	public IPositionProvider setNewParent(EObject parent, PosAndSize invisibleRect, PosAndSize innerRect);
	
	public boolean contains(EObject obj);
	public PosAndSize getDiagramPosition();
	public PosAndSize getPosition(EObject bo);
	public Pos getConnectionText(EObject bo);
	public List<Pos> getConnectionBendpoints(EObject bo);
}
