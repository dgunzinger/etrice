// @ts-check

const lightCodeTheme = require('prism-react-renderer/themes/vsLight');
const darkCodeTheme = require('prism-react-renderer/themes/vsDark');
const packageJson = require('./package.json');

/** @type {import('@docusaurus/types').Config} */
const config = {
	title: 'eTrice Documentation',
	tagline: 'Real-Time Modeling Tools',

	url: 'https://eclipse.dev',
	baseUrl: '/etrice/documentation/release',
	
	markdown: {
		// process .md files as commonmark opposed to mdx - only available in the upcoming docusaurus version 3 and should make upgrading easy
		// format: 'detect'
	},

	onBrokenLinks: 'throw',
	onBrokenMarkdownLinks: 'throw',

	presets: [
		[
			'classic',
			/** @type {import('@docusaurus/preset-classic').Options} */
			({
				docs: {
					routeBasePath: '/',
					sidebarPath: require.resolve('./sidebars.js'),
					exclude: ['SUMMARY.md'],
					editUrl: 'https://gitlab.eclipse.org/eclipse/etrice/etrice/-/blob/master/plugins/org.eclipse.etrice.doc',
				},
				blog: false,
			}),
		],
	],

	themeConfig:
		/** @type {import('@docusaurus/preset-classic').ThemeConfig} */
		({
			navbar: {
				logo: {
					alt: 'eTrice',
					src: 'img/etrice-logo-cmyk.png',
				},
				items: [
					{
						type: 'docSidebar',
						position: 'left',
						sidebarId: 'docs',
						label: 'Docs',
					},
					{
						type: 'html',
						value: 'Version ' + packageJson.version,
						position: 'right'
					},
					{
						href: 'https://eclipse.dev/etrice',
						label: 'eTrice Home',
						position: 'right'
					},
					{
						href: 'https://gitlab.eclipse.org/eclipse/etrice/etrice',
						label: 'GitLab',
						position: 'right',
					},
				],
			},
			footer: {
				copyright: `Copyright © Eclipse Foundation. All Rights Reserved. Built with Docusaurus.`,
			},
			prism: {
				theme: lightCodeTheme,
				darkTheme: darkCodeTheme,
			}
		}),

		plugins: [require.resolve('docusaurus-lunr-search')]
};

module.exports = config;
