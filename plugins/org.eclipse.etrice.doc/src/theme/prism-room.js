Prism.languages.room = {
	'comment': {
		pattern: /(?:\/\*[\s\S]*?\*\/)|(?:\/\/.*)/,
		greedy: true
	},
	'string': {
		pattern: /"(?:\\(?:\r\n|[\s\S])|[^"\\\r\n])*"/,
		greedy: true
	},
	'ccstring': {
		pattern: /'''[\s\S]*?'''/,
		greedy: true,
		lookbehind: true,
		inside: {
			'string': /'''/,
			'language-clike': {
				pattern: /[\s\S]*/,
				inside: Prism.languages['clike']
			}
		}
	},
	'punctuation': /[{}()\[\]<>.,:]/,
	'operator': /=|->/,
	'number': /\b\d+\b/,
	'keyword': /\b(?:handler|TransitionPoint|LogicalSystem|ActorRef|ExitPoint|semantics|cond|action|PrimitiveType|model|usercode|satisfied_by|RefinedState|in|handle|optional|triggers|exit|RefinedTransition|PortClass|ctor|RoomModel|Attribute|guard|mandatory|AnnotationType|out|default|override|attribute|dtor|Structure|regular|SubSystemRef|Enumeration|conjugated|abstract|LogicalThread|cp|sends|SubSystemClass|LayerConnection|subgraph|ServiceImplementation|ActorInstanceMapping|Transition|eventdriven|Interface|Port|usercode2|usercode3|EntryPoint|usercode1|do|ref|SPP|ActorBehavior|from|Behavior|varargs|Binding|incoming|my|sync|entry|datadriven|external|extends|State|CompoundProtocolClass|fixed|ActorClass|outgoing|private|SAP|Message|import|StateMachine|Operation|ProtocolClass|ExternalType|async|DataClass|relay_sap|ChoicePoint|and|or|of)\b/,
	'builtin': /\b(?:void|boolean|char|int8|int16|int32|int64|uint8|uint16|uint32|uint64|float32|float64|ptReal|ptInteger|ptCharacter|ptBoolean)\b/,
	'constant': /\b(?:initial)\b/,
	'boolean': /\b(?:false|true)\b/
};
