Architecture
============

The basic components of eTrice are depicted in the following diagram.

![image](../images/200-components.jpg)

Additional to that the eTrice project comprises runtime libraries and unit tests which are treated in subsequent sections.

## Editor and Generator Components

-   core

    -   core.common is an Xtext based language which serves as a base for other eTrice languages. It consists of the plug-ins `org.eclipse.etrice.core.common` and `org.eclipse.etrice.core.common.ui`. The base grammar defines recurring items like numbers with literals, annotations and the like.

    -   core.fsm is an Xtext based language that defines state machines in an abstract way. It consists of the plug-ins `org.eclipse.etrice.core.fsm` and `org.eclipse.etrice.core.fsm.ui`. The FSM language is abstract and has to be embedded in a model that defines containers for the state machine with interface items (e.g. ROOM ports or Franca interfaces) and messages. The ROOM grammar of eTrice is derived from this grammar.

    -   core.room is an Xtext based language called ROOM. It consists of the plug-ins `org.eclipse.etrice.core.room` and `org.eclipse.etrice.core.room.ui`. ROOM is the basic modeling language of eTrice.

    -   core.config is an Xtext based language called Config. It consists of the plug-ins `org.eclipse.etrice.core.config` and `org.eclipse.etrice.core.config.ui`. Config is a language designed for the data configuration of model

    -   core.etphys is an Xtext based language called etPhys. It consists of the plug-ins `org.eclipse.etrice.core.etphys` and `org.eclipse.etrice.core.etphys.ui`. etPhys is a language designed for the description of physical systems onto which the logical ROOM systems are deployed.

    -   core.etmap is an Xtext based language called etMap. It consists of the plug-ins `org.eclipse.etrice.core.etmap` and `org.eclipse.etrice.core.etmap.ui`. etMap is a language designed for the mapping of logical to physical systems.

    -   core.genmodel.fsm is an EMF based aggregation layer for finite state machines. It consists of the plugin `org.eclipse.etrice.core.genmodel.fsm`. A `ModelComponent` can be transformed into a `ExpandedModelComponent` which is an explicit version of the state machine with all the inherited items contained.

    -   core.genmodel is an EMF based aggregation layer for Room models. It consists of the plugin `org.eclipse.etrice.core.genmodel`. A Room model can be transformed into a genmodel which allows easy access to implicit relations of the Room model.

-   ui

    -   textual

        -   fsm.ui is the ui counterpart of core.fsm. It consists of the plug-in `org.eclipse.etrice.core.fsm.ui`. This plug-in realizes IDE concepts like content assist, error markers and navigation by hyper links for the FSM language.

        -   room.ui is the ui counterpart of core.room. It consists of the plug-in `org.eclipse.etrice.core.room.ui`. This plug-in realizes IDE concepts like content assist, error markers and navigation by hyper links for the Room language.

        -   config.ui is the ui counterpart of core.config. It consists of the plug-in `org.eclipse.etrice.core.config.ui`. This plug-in realizes IDE concepts like content assist, error markers and navigation by hyper links for the Config language.

        -   etphys.ui is the ui counterpart of core.etphys. It consists of the plug-in `org.eclipse.etrice.core.etphys.ui`. This plug-in realizes IDE concepts like content assist, error markers and navigation by hyper links for the etPhys language.

        -   etmap.ui is the ui counterpart of core.etmap. It consists of the plug-in `org.eclipse.etrice.core.etmap.ui`. This plug-in realizes IDE concepts like content assist, error markers and navigation by hyper links for the etPhys language.

    -   graphical

        -   ui.common.base is a set of common code for the diagram editors. It consists of the plug-in `org.eclipse.etrice.ui.common.base`. It depends only on the FSM part but not on ROOM.

        -   ui.common is a set of common code for the two diagram editors. It consists of the plug-in `org.eclipse.etrice.ui.common`.

        -   ui.commands encapsulates some commands related to the navigation between eTrice editors. It consists of the plug-in `org.eclipse.etrice.ui.commands`.

        -   ui.structure is the Graphiti based editor for the Actor structure. It consists of the plug-in `org.eclipse.etrice.ui.structure`.

        -   ui.behavior.fsm is implementing the major part for the graphical state machine editor. It consists of the plug-in `org.eclipse.etrice.ui.behavior.fsm`. All property dialogs are handled in an abstract way using a factory.

        -   ui.behavior is the Graphiti based editor for the Actor behavior. It consists of the plug-in `org.eclipse.etrice.ui.behavior`. It utilizes the ui.behavior.fsm and provides concrete property dialogs.

-   generators

    -   generator.fsm is a set of general classes and language independent parts of all generators. It consists of the plug-in *org.eclipse.etrice.generator.fsm*. It depends only on FSM but not on ROOM.

    -   generator is a set of general classes and language independent parts of all generators. It consists of the plug-in *org.eclipse.etrice.generator*.

    -   generator.c is the generator for the ANSI-C target language. It consists of the plug-in `org.eclipse.etrice.generator.c`.

    -   generator.cpp is the generator for the C++ target language. It consists of the plug-in `org.eclipse.etrice.generator.cpp`.

    -   generator.java is the generator for the Java target language. It consists of the plug-in `org.eclipse.etrice.generator.java`.

    -   generator.doc is the generator for the model documentation. It consists of the plug-in `org.eclipse.etrice.generator.doc`.

## The Abstract Finite State Machine Concept

eTrice comes with an easy to re-use concept of hierarchical finite state machines (FSM for short). A powerful inheritance concept is used and there is also state machine validation based on semantic rules for messages and abstract execution available.

State machines are an integral part of the ROOM language. But they can also be used independently from that using

-   for the model part

    -   `org.eclipse.etrice.core.common`

    -   `org.eclipse.etrice.core.fsm`

    -   `org.eclipse.etrice.core.genmodel.fsm`

-   graphical state machine editor

    -   `org.eclipse.etrice.core.common.ui`

    -   `org.eclipse.etrice.core.fsm.ui`

    -   `org.eclipse.etrice.core.ui.common.base`

    -   `org.eclipse.etrice.core.ui.common`

-   base classes for code generation

    -   `org.eclipse.etrice.generator.fsm`

-   validation by abstract execution

    -   `org.eclipse.etrice.abstractexec.behavior`

The first three parts have to be used by concrete implementations that implement the abstract interface. eTrice itself uses the abstract FSMs in exactly this way.

### Extending the FSM Model

The eTrice FSM model has to be embedded in a model that introduces components, interfaces and messages. We recommend to use a new Xtext language with a grammar derived from the FSM grammar. This grammar has to specify a component derived from the `ModelComponent` of the FSM model. It further has to introduce concrete realizations of interface items derived from `AbstractInterfaceItem`. The interface item is an object contained in a component that has a name (role) and holds a reference to some kind of interface of the component (like a Franca interface or a ROOM protocol). Finally a concrete message type derived from an `EObject` has to be defined. The minimal requirement is that this concrete message has an attribute called ’name’ of type String.

The minimal interface to be implemented consists of

-   for the concrete interface item

    -   `EList<EObject> getAllIncomingAbstractMessages()`

    -   `EList<EObject> getAllOutgoingAbstractMessages()`

    -   `ProtocolSemantics getSemantics()`

-   for the concrete model component

    -   `EList<AbstractInterfaceItem> getAbstractInterfaceItems` – the interface items contained in this model component

    -   `EList<AbstractInterfaceItem> getAllAbstractInterfaceItems` – all interface items including inherited ones

    -   `String getComponentName()` – should return the name of the model component

### Extending the State Machine Editor

The concrete state machine editor minimally needs to define

-   the editor class itself by deriving it from the `AbstractFSMEditor`

-   a diagram type provider (which may derive from `AbstractDiagramTypeProvider`)

-   a Google Guice module with bindings for

    -   `IFSMDialogFactory`

    -   `DiagramAccessBase`

    -   `IBehaviorQuickfixProvider`

    -   `IResourceSetProvider`

-   concrete implementations of all property dialogs the `IFSMDialogFactory` produces

## Runtimes

Currently eTrice ships with a C and a Java runtime. The C++ runtime is still a prototype. The runtimes are libraries written in the target language against which the generated code is compiled.

## Unit Tests

Most plug-ins and other parts of the code have related unit tests.
