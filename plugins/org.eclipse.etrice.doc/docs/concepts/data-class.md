DataClass
=========

## Description

The DataClass allows the modeling of hierarchical complex data types and operations on them. The data class is the equivalent to a class in languages like Java or C++, but has less features. The content of a data class can always be sent via message between actors (defined as message data in a ProtocolClass).

## Notation

Example: DataClass using PrimitiveTypes

```room
DataClass DataClass1 {
	Attribute attribute1: int32    // attribute of primitive type
	Attribute attribute2: float32  // attribute of another primitive type

	// no arguments, no return value
	Operation operation1(): void '''
		user code
	'''
	// argument of primitive type, no return value
	Operation operation2(Param1: int32): void '''
		user code
	'''
	// argument of primitive type, return value of primitive type
	Operation operation3(Param1: int32): float64 '''
		user code
	'''
}
```
Example: DataClass using other DataClasses:

```room
DataClass DataClass2 {
	Attribute attribute1: int32      // attribute of primitive type
	Attribute attribute2: DataClass1 // attribute of DataClass

	// arguments and return value by value
	Operation operation1(Param1: int32, Param2: DataClass1): DataClass1 '''
		user code
	'''
	// arguments and return value by reference except for primitive types
	Operation operation2(Param1: int32, Param2: DataClass1 ref): DataClass1 ref '''
		user code
	'''
}
```
