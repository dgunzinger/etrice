Examples
========

The eTrice examples can be installed using the new wizard.

Choose File &gt; New &gt; Other (or Ctrl-N), open category “eTrice”. There are examples for C and for Java. Each of them consists of a single Eclipse project that contains several examples which are described in the next sections. Select the desired one, click Next and Finish and you are ready to go.

As already mentioned each example project contains several examples. Each of them comes with a room model, a mapping model a launch configuration for the code generation and a launch configuration to run the compiled example.
