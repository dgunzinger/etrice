/*******************************************************************************
 * Copyright (c) 2010 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Christian Hilden (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ui.behavior.fsm.support.util

import org.eclipse.graphiti.mm.pictograms.ContainerShape
import org.eclipse.etrice.ui.behavior.fsm.support.StateGraphSupport
import org.eclipse.graphiti.mm.pictograms.Shape
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.services.Graphiti
import org.eclipse.etrice.core.fsm.fSM.TransitionPoint
import org.eclipse.etrice.core.fsm.fSM.EntryPoint
import org.eclipse.etrice.core.fsm.fSM.ExitPoint
import org.eclipse.etrice.core.fsm.fSM.State

class FSMSizeAndPositionUtil {
	
	/**
	 * Ensured the StateGraphContainer has enough size for all contained shapes 
	 */
	static def void resizeDiagramFrame(ContainerShape sgShape) {
		var minWidth = StateGraphSupport.DEFAULT_SIZE_X;
		var minHeight = StateGraphSupport.DEFAULT_SIZE_Y;
		for (Shape child : sgShape.getChildren()) {
			val ga = child.getGraphicsAlgorithm();
			minWidth = Math.max(minWidth, ga.getWidth() + ga.getX());
			minHeight = Math.max(minHeight, ga.getHeight() + ga.getY());
		}
		
		val sgGa = sgShape.graphicsAlgorithm;
		
		// GA children != shape children, first ga child of sgShape is rounded rect
		// There are 2 rounded rect children. One with alpha filter + one with non alpha frame
		
		val gaRoundedRect1 = sgGa.getGraphicsAlgorithmChildren().get(0);
		val gaRoundedRect2 = sgGa.getGraphicsAlgorithmChildren().get(1);
		
		val oldFrameWidth = gaRoundedRect1.width;
		val oldFrameHeight = gaRoundedRect1.height;
				
		
		if (sgGa.width < minWidth) {
			sgGa.setWidth(minWidth + StateGraphSupport.MARGIN * 2);
			gaRoundedRect1.setWidth(minWidth + StateGraphSupport.MARGIN);
			gaRoundedRect2.setWidth(minWidth + StateGraphSupport.MARGIN);
		}
		
		if (sgGa.height < minHeight) {
			sgGa.setHeight(minHeight + StateGraphSupport.MARGIN * 2);
			gaRoundedRect1.setHeight(minHeight+ StateGraphSupport.MARGIN);
			gaRoundedRect2.setHeight(minHeight+ StateGraphSupport.MARGIN);
		}
		
		// Fix all nodes which are stuck to the container border (transition points)
		// For some reason the border and distances behave differently depending if we are in the root element or a SubStatemachine 
		val sg = Graphiti.linkService.getBusinessObjectForLinkedPictogramElement(sgShape as PictogramElement);		
		val frameOffset = if (sg.eContainer instanceof State) StateGraphSupport.MARGIN else 0;
		fixBorderNodesAfterResize(sgShape, oldFrameWidth, oldFrameHeight, gaRoundedRect1.width, gaRoundedRect1.height - frameOffset);

	}
	
	static def void fixBorderNodesAfterResize(
		ContainerShape sgShape
		, int oldFrameWidth
		, int oldFrameHeight
		, int newFrameWidth
		, int newFrameHeight
	) {
		for (Shape child : sgShape.getChildren()) {
			val bo = Graphiti.linkService.getBusinessObjectForLinkedPictogramElement(child as PictogramElement)
			if (bo instanceof TransitionPoint || bo instanceof EntryPoint || bo instanceof ExitPoint) {
				if (child.graphicsAlgorithm.x == oldFrameWidth) {
					// right border
					child.graphicsAlgorithm.x = newFrameWidth
				} else if (child.graphicsAlgorithm.y == oldFrameHeight) {
					// bottom border
					child.graphicsAlgorithm.y = newFrameHeight
				}				
			}
		}
	}
	
	
	
}