package org.eclipse.etrice.core

import org.eclipse.etrice.core.tests.RoomInjectorProvider
import org.eclipse.xtext.testing.XtextRunner
import org.junit.runner.RunWith
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider
import com.google.inject.Inject
import org.junit.Test
import org.eclipse.etrice.core.common.documentation.DocumentationMarkup
import org.eclipse.etrice.core.common.documentation.CommonMarkParser

import static org.junit.Assert.*
import org.junit.Before
import org.eclipse.etrice.core.room.RoomModel

@RunWith(XtextRunner)
@InjectWith(RoomInjectorProvider)
class CommentsTest extends TestBase {
	
	@Inject IEObjectDocumentationProvider docProvider
	
	@Before
	def void setUp() {
		prepare(CoreTestsActivator.getInstance().getBundle())
	}

	@Test
	def void commonMarkComments() {
		val model = getResource('Comments.room').contents.head as RoomModel
		val asteriskPrefix = docProvider.getDocumentation(model.roomClasses.findFirst[name == 'AsteriskPrefix'])
		val noAsteriskPrefix = docProvider.getDocumentation(model.roomClasses.findFirst[name == 'NoAsteriskPrefix'])

		assertEquals(DocumentationMarkup.MARKUP_COMMONMARK, DocumentationMarkup.getMarkupType(asteriskPrefix))
		assertEquals(DocumentationMarkup.MARKUP_COMMONMARK, DocumentationMarkup.getMarkupType(noAsteriskPrefix))
		val expected = '''
			<p>https://spec.commonmark.org/0.30/#hard-line-breaks</p>
			<p>Hard line break backslash<br />
			Hard line break 2 spaces<br />
			This is a new line</p>
			<ul>
			<li>bullet list
			<ul>
			<li>nested list</li>
			</ul>
			</li>
			</ul>
			<pre><code>  code *
			</code></pre>
		'''.toString.trim.replace('\r\n', '\n')
		assertEquals(expected, CommonMarkParser.toHtml(asteriskPrefix).trim)
		assertEquals(expected, CommonMarkParser.toHtml(noAsteriskPrefix).trim)
	}
}