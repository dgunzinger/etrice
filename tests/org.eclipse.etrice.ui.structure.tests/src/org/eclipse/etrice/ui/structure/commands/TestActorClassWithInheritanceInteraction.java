/*******************************************************************************
 * Copyright (c) 2022 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * CONTRIBUTORS:
 * 		Eyrak Paen-Rochlitz (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.ui.structure.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.ActorRef;
import org.eclipse.etrice.core.room.Binding;
import org.eclipse.etrice.core.room.InterfaceItem;
import org.eclipse.etrice.core.room.Port;
import org.eclipse.etrice.core.room.util.RoomHelpers;
import org.eclipse.etrice.tests.base.TestBase;
import org.eclipse.etrice.ui.structure.DiagramAccess;
import org.eclipse.etrice.ui.structure.StructureTestActivator;
import org.eclipse.etrice.ui.structure.support.DiagramUtil;
import org.eclipse.etrice.ui.structure.support.Pos;
import org.eclipse.etrice.ui.structure.support.PosAndSize;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.impl.AddBendpointContext;
import org.eclipse.graphiti.features.context.impl.MoveBendpointContext;
import org.eclipse.graphiti.features.context.impl.MoveShapeContext;
import org.eclipse.graphiti.features.context.impl.RemoveBendpointContext;
import org.eclipse.graphiti.features.context.impl.ResizeShapeContext;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for the diagram feature interaction relating to inheritance
 */
public class TestActorClassWithInheritanceInteraction extends TestBase {

	private ActorClass acBase;
	private ActorClass ac;
	private ActorRef aref;
	private InterfaceItem arefPort;
	private Port portBound;
	private Port portRight;
	private Port portBottom;
	private Binding binding;
	private IFeatureProvider fp;
	
	private RoomHelpers roomHelpers = new RoomHelpers();

	@Before
	public void setUp() throws IOException {
		loadModelFile();

		assertEquals("models read", 1, getModels().size());
		assertEquals("actor classes in our model", 3, getActorClasses().count());

		ac = getActorClass("ac");
		assertNotNull("ActorClass expected", ac);
		acBase = getActorClass("ac_base");
		assertNotNull("ActorClass expected", acBase);
		aref = acBase.getActorRefs().get(0);
		arefPort = roomHelpers.getInterfaceItems(aref.getStructureClass(), false).get(0);
		portBound = acBase.getInterfacePorts().stream().filter(p -> p.getName().equals("p_base")).findFirst().orElseThrow();
		portRight = acBase.getInterfacePorts().stream().filter(p -> p.getName().equals("p_base_right")).findFirst().orElseThrow();
		portBottom = acBase.getInterfacePorts().stream().filter(p -> p.getName().equals("p_base_bottom")).findFirst().orElseThrow();
		binding = acBase.getBindings().get(0);
		
		// Modify elements in base diagram to initial positions
		Diagram diagramBase = new DiagramAccess().getDiagram(acBase);
		fp = GraphitiUi.getExtensionManager().createFeatureProvider(diagramBase);
		
		{
			ResizeShapeContext ctx = new ResizeShapeContext(getFirstContainerShape(diagramBase, aref));
			ctx.setLocation(100, 100);
			ctx.setSize(300, 300);
			IFeature feature = fp.getResizeShapeFeature(ctx);
			assertTrue(feature.canExecute(ctx));
			feature.execute(ctx);
		}
		
		{
			ContainerShape container = getFirstContainerShape(diagramBase, acBase);
			PosAndSize extent = DiagramUtil.getVisibleExtent(container);
			MoveShapeContext ctx = new MoveShapeContext(getFirstContainerShape(diagramBase, portRight));
			moveTo(ctx, extent.getX() + extent.getW(), 100);
			ctx.setSourceContainer(container);
			ctx.setTargetContainer(container);
			IFeature feature = fp.getMoveShapeFeature(ctx);
			assertTrue(feature.canExecute(ctx));
			feature.execute(ctx);
		}
		
		{
			ContainerShape container = getFirstContainerShape(diagramBase, acBase);
			PosAndSize extent = DiagramUtil.getVisibleExtent(container);
			MoveShapeContext ctx = new MoveShapeContext(getFirstContainerShape(diagramBase, portBottom));
			moveTo(ctx, 100, extent.getY() + extent.getH());
			ctx.setSourceContainer(container);
			ctx.setTargetContainer(container);
			IFeature feature = fp.getMoveShapeFeature(ctx);
			assertTrue(feature.canExecute(ctx));
			feature.execute(ctx);
		}
		
		{
			AddBendpointContext ctx = new AddBendpointContext(getFirstConnection(diagramBase, binding), 150, 150, 0);
			IFeature feature = fp.getAddBendpointFeature(ctx);
			assertTrue(feature.canExecute(ctx));
			feature.execute(ctx);
		}
		
		{
			ResizeShapeContext ctx = new ResizeShapeContext(getFirstContainerShape(diagramBase, acBase));
			ctx.setLocation(40, 40);
			ctx.setSize(600, 600);
			IFeature feature = fp.getResizeShapeFeature(ctx);
			assertTrue(feature.canExecute(ctx));
			feature.execute(ctx);
		}
		
		diagramBase.eResource().save(Collections.emptyMap());
	}
	
	@After
	public void tearDown() {
		removeDiagramsDirectory();
	}
	
	@Override
	protected String getModelFileName() {
		return "ActorClassWithInheritance2.room";
	}
	
	@Test
	public void checkInheritedElementPositions() {
		Diagram diagram = new DiagramAccess().getDiagram(ac);
		Diagram diagramBase = new DiagramAccess().getDiagram(acBase);
		assertEquals(DiagramUtil.getVisibleExtent(diagramBase), DiagramUtil.getVisibleExtent(diagram));
		assertEquals(getVisibleExtent(diagramBase, aref), getVisibleExtent(diagram, aref));
		assertEquals(getVisibleExtent(diagramBase, portBound), getVisibleExtent(diagram, portBound));
		assertEquals(getVisibleExtent(diagramBase, portRight), getVisibleExtent(diagram, portRight));
		assertEquals(getVisibleExtent(diagramBase, portBottom), getVisibleExtent(diagram, portBottom));
		
		List<Point> bendPoints = getFirstConnection(diagram, binding).getBendpoints();
		List<Point> bendPointsBase = getFirstConnection(diagramBase, binding).getBendpoints();
		assertEquals(1, bendPoints.size());
		assertEquals(1, bendPointsBase.size());
		assertEquals(bendPointsBase.get(0).getX(), bendPoints.get(0).getX());
		assertEquals(bendPointsBase.get(0).getY(), bendPoints.get(0).getY());
	}

	@Test
	public void checkInheritedElementsFixed() {
		Diagram diagram = new DiagramAccess().getDiagram(ac);
		
		{
			// try move aref
			ContainerShape container = getFirstContainerShape(diagram, ac);
			MoveShapeContext ctx = new MoveShapeContext(getFirstContainerShape(diagram, aref));
			moveTo(ctx, 200, 200);
			ctx.setSourceContainer(container);
			ctx.setTargetContainer(container);
			IFeature feature = fp.getMoveShapeFeature(ctx);
			assertFalse(feature.canExecute(ctx));
		}
		
		{
			// try move aref port
			ContainerShape container = getFirstContainerShape(diagram, aref);
			MoveShapeContext ctx = new MoveShapeContext(getFirstContainerShape(diagram, arefPort));
			moveByDelta(ctx, 50, 0);
			ctx.setSourceContainer(container);
			ctx.setTargetContainer(container);
			IFeature feature = fp.getMoveShapeFeature(ctx);
			assertFalse(feature.canExecute(ctx));
		}
		
		{
			// try resize aref
			ResizeShapeContext ctx = new ResizeShapeContext(getFirstContainerShape(diagram, aref));
			ctx.setLocation(200, 200);
			ctx.setSize(400, 400);
			IFeature feature = fp.getResizeShapeFeature(ctx);
			assertFalse(feature.canExecute(ctx));
		}
		
		{
			// try move port
			ContainerShape container = getFirstContainerShape(diagram, ac);
			MoveShapeContext ctx = new MoveShapeContext(getFirstContainerShape(diagram, portRight));
			moveByDelta(ctx, 0, 100);
			ctx.setSourceContainer(container);
			ctx.setTargetContainer(container);
			IFeature feature = fp.getMoveShapeFeature(ctx);
			assertFalse(feature.canExecute(ctx));
		}
		
		{
			// try add bendpoint
			AddBendpointContext ctx = new AddBendpointContext(getFirstConnection(diagram, binding), 100, 100, 0);
			IFeature feature = fp.getAddBendpointFeature(ctx);
			assertFalse(feature.canExecute(ctx));
		}
		
		{
			// try move bendpoint
			FreeFormConnection conn = getFirstConnection(diagram, binding);
			MoveBendpointContext ctx = new MoveBendpointContext(conn.getBendpoints().get(0));
			ctx.setConnection(conn);
			ctx.setLocation(450, 150);
			IFeature feature = fp.getMoveBendpointFeature(ctx);
			assertFalse(feature.canExecute(ctx));
		}
		
		{
			// try remove bendpoint
			FreeFormConnection conn = getFirstConnection(diagram, binding);
			RemoveBendpointContext ctx = new RemoveBendpointContext(getFirstConnection(diagram, binding), conn.getBendpoints().get(0));
			IFeature feature = fp.getRemoveBendpointFeature(ctx);
			assertFalse(feature.canExecute(ctx));
		}
	}
	
	@Test
	public void checkInheritedPositionsAfterResize() {
		Diagram diagram = new DiagramAccess().getDiagram(ac);
		final int DELTA_X = 400;
		PosAndSize origAref = getPosAndSize(diagram, aref);
		PosAndSize origArefPort = getPosAndSize(diagram, arefPort);
		PosAndSize origPortBound = getPosAndSize(diagram, portBound);
		PosAndSize origPortBottom = getPosAndSize(diagram, portBottom);
		PosAndSize origPortRight = getPosAndSize(diagram, portRight);
		Point bendpoint = getFirstConnection(diagram, binding).getBendpoints().get(0);
		Pos origBendpoint = new Pos(bendpoint.getX(), bendpoint.getY());
		
		{
			// do resize
			ResizeShapeContext ctx = new ResizeShapeContext(getFirstContainerShape(diagram, ac));
			ctx.setLocation(ctx.getShape().getGraphicsAlgorithm().getX(), ctx.getShape().getGraphicsAlgorithm().getY());
			ctx.setSize(DELTA_X + ctx.getShape().getGraphicsAlgorithm().getWidth(), ctx.getShape().getGraphicsAlgorithm().getHeight());
			IFeature feature = fp.getResizeShapeFeature(ctx);
			assertTrue(feature.canExecute(ctx));
			feature.execute(ctx);
		}
		
		{
			// check inner elements unchanged
			PosAndSize actualAref = getPosAndSize(diagram, aref);
			PosAndSize actualArefPort = getPosAndSize(diagram, arefPort);
			assertEquals(origAref, actualAref);
			assertEquals(origArefPort, actualArefPort);
		}
		
		{
			// check unchanged ports
			PosAndSize actualPortBound = getPosAndSize(diagram, portBound);
			PosAndSize actualPortBottom = getPosAndSize(diagram, portBottom);
			assertEquals(origPortBound, actualPortBound);
			assertEquals(origPortBottom, actualPortBottom);
		}
		
		{
			// check right port follows border
			PosAndSize expectedPortRight = new PosAndSize(origPortRight.getX() + DELTA_X, origPortRight.getY(),
					origPortRight.getW(), origPortRight.getH());
			PosAndSize actualPortRight = getPosAndSize(diagram, portRight);
			assertEquals(expectedPortRight, actualPortRight);
		}
		
		{
			// check bendpoint unchanged
			bendpoint = getFirstConnection(diagram, binding).getBendpoints().get(0);
			Pos actualBendpoint = new Pos(bendpoint.getX(), bendpoint.getY());
			assertEquals(origBendpoint, actualBendpoint);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.etrice.tests.base.TestBase#getModelsDirectoy()
	 */
	@Override
	protected URL getModelsDirectoy() {
		return StructureTestActivator.getInstance().getBundle().getEntry("models");
	}

	private <T extends PictogramElement> T getFirstPEOfType(Class<T> type, Diagram diagram, EObject bo) {
		List<PictogramElement> all = Graphiti.getLinkService().getPictogramElements(diagram, bo);
		T result = all.stream().filter(type::isInstance).map(type::cast).filter(pe -> !Diagram.class.isInstance(pe)).findFirst().orElse(null);
		return result;
	}

	private ContainerShape getFirstContainerShape(Diagram diagram, EObject bo) {
		return getFirstPEOfType(ContainerShape.class, diagram, bo);
	}

	private FreeFormConnection getFirstConnection(Diagram diagram, EObject bo) {
		return getFirstPEOfType(FreeFormConnection.class, diagram, bo);
	}

	protected void moveTo(MoveShapeContext ctx, int x, int y) {
		ctx.setX(x);
		ctx.setY(y);
		ctx.setDeltaX(x - ctx.getShape().getGraphicsAlgorithm().getX());
		ctx.setDeltaX(y - ctx.getShape().getGraphicsAlgorithm().getY());
	}
	
	protected void moveByDelta(MoveShapeContext ctx, int x, int y) {
		ctx.setDeltaX(x);
		ctx.setDeltaY(y);
		ctx.setX(x + ctx.getShape().getGraphicsAlgorithm().getX());
		ctx.setY(y + ctx.getShape().getGraphicsAlgorithm().getY());
	}
	
	protected PosAndSize getVisibleExtent(Diagram diagram, EObject bo) {
		return DiagramUtil.getVisibleExtent(getFirstContainerShape(diagram, bo));
	}
	
	protected PosAndSize getPosAndSize(Diagram diagram, EObject bo) {
		ContainerShape shape = getFirstContainerShape(diagram, bo);
		return DiagramUtil.getPosAndSize(shape.getGraphicsAlgorithm());
	}
}
