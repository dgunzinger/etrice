/*******************************************************************************
 * Copyright (c) 2011 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * CONTRIBUTORS:
 * 		Thomas Schuetz (initial contribution)
 *
 *******************************************************************************/


/*
 * TestEtUnit.c
 *
 *  Created on: 16.01.2012
 *      Author: tschuetz
 */

#include "TestEtUnit.h"
#include "etUnit/etUnit.h"

#include <assert.h>

void TestEtUnit_Expect(etInt16 id){
	EXPECT_TRUE(id, "EXPECT_TRUE", ET_TRUE);
	EXPECT_FALSE(id, "EXPECT_FALSE", ET_FALSE);

	/* signed integer values */
	EXPECT_EQUAL_INT8(id, "EXPECT_EQUAL_INT8", -123, -123);
	EXPECT_EQUAL_INT16(id, "EXPECT_EQUAL_INT16", -12345, -12345);
	EXPECT_EQUAL_INT32(id, "EXPECT_EQUAL_INT32", -1234567, -1234567);

	/* unsigned integer values */
	EXPECT_EQUAL_UINT8(id, "EXPECT_EQUAL_INT8", 123, 123);
	EXPECT_EQUAL_UINT16(id, "EXPECT_EQUAL_INT16", 12345, 12345);
	EXPECT_EQUAL_UINT32(id, "EXPECT_EQUAL_INT32", 1234567, 1234567);

	/* float values */
#ifdef ET_FLOAT32
	EXPECT_EQUAL_FLOAT32(id, "EXPECT_EQUAL_FLOAT32", (etFloat32) 123.456, (etFloat32) 123.456, (etFloat32) 0.0001);
	EXPECT_EQUAL_FLOAT32(id, "EXPECT_EQUAL_FLOAT32", (etFloat32) 123.456, (etFloat32) 123.456, (etFloat32) 0.0001);
#endif

	/* Pointers */
	etUInt16 value;
	etUInt16* valuePtr = &value;

	EXPECT_EQUAL_PTR(id, "EXPECT_EQUAL_PTR", &value, valuePtr);

	char str1[] = "abc";
	char str2[] = "abc";
	EXPECT_EQUAL_STR(id, "EXPECT_EQUAL_STR", str1, str2);
}


void TestEtUnit_Expect_Order(etInt16 id){
	etInt16 list[] = {1,2,3,4};
	EXPECT_ORDER_START(id, list, 4);
	EXPECT_ORDER(id, "id=1", 1);
	EXPECT_ORDER(id, "id=2", 2);
	EXPECT_ORDER(id, "id=3", 3);
	EXPECT_ORDER_END(id, "id=4", 4);
}

void TestEtUnit_Parallel_TestCase(){
	etUnit_setParallelTestCases(true);
	{
		etInt16 id1 = etUnit_openTestCase("Parallel_Tc_1");
		etInt16 id2 = etUnit_openTestCase("Parallel_Tc_2");

		assert(id1 > 0);
		EXPECT_TRUE(id1, "id > 0", id1 > 0);
		assert(id2 > 0);
		EXPECT_TRUE(id2, "id > 0", id2 > 0);
		assert(id2 == id1 + 1);
		EXPECT_TRUE(id2, "id incremented", id2 == id1 + 1);

		etUnit_closeTestCase(id1);
		etUnit_closeTestCase(id2);
	}
	etUnit_setParallelTestCases(false);
}

void TestEtUnit_Singleton_TestCase(){
	etInt16 id = etUnit_openTestCase("Singleton_Tc_1");
	assert(id == 0);
	ETUNIT_EQUAL_INT("id=0", id, 0);
	etUnit_closeTestCase(0);

	id = etUnit_openTestCase("Singleton_Tc_2");
	assert(id == 0);
	ETUNIT_FALSE("id=0", id);
	etUnit_closeTestCase(0);
}

void TestEtUnit_Skip_TestCase(){
	etInt16 id = etUnit_openTestCase("SkipTestcase");
	assert(id == 0);
	etUnit_skipTestCase(0, "skip msg");
}

void TestEtUnit_timeInMillis32(etInt16 id) {
	etInt32 result = 0;

	{
		const etTime t = { .sec = 123, .nSec = 23000000 };
		ETUNIT_TRUE("positive time", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("positive time result", 123023, result);
	}

	{
		result = 42;
		ETUNIT_FALSE("invalid input param", etUnit_timeInMillis32(NULL, &result));
		ETUNIT_TRUE("invalid input param result modification", 42 == result);
	}

	{
		result = 42;
		const etTime t = { .sec = 123, .nSec = 23000000 };
		ETUNIT_FALSE("invalid result param", etUnit_timeInMillis32(&t, NULL));
		ETUNIT_TRUE("invalid input param result modification", 42 == result);
	}

	{
		const etTime t = { .sec = -123, .nSec = 23000000 };
		ETUNIT_TRUE("negative time", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("negative time result", -122977, result);
	}

	{
		const etTime t = { .sec = 2147483, .nSec = 647000000 };
		ETUNIT_TRUE("max range time", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("max range time result", INT32_MAX, result);
	}
	
	{
		const etTime t = { .sec = -2147484, .nSec = 352000000 };
		ETUNIT_TRUE("min range time", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("min range time result", INT32_MIN, result);
	}

	{
		result = 42;
		const etTime t = { .sec = 2147483, .nSec = 648000000 };
		ETUNIT_FALSE("out of range positive", etUnit_timeInMillis32(&t, &result));
		ETUNIT_TRUE("out of range positive result modification", 42 == result);
	}

	{
		result = 42;
		const etTime t = { .sec = -2147484, .nSec = 351000000 };
		ETUNIT_FALSE("out of range negative", etUnit_timeInMillis32(&t, &result));
		ETUNIT_TRUE("out of range negative result modification", 42 == result);
	}

	{
		const etTime t = { .sec = 123, .nSec = 23789789 };
		ETUNIT_TRUE("round millis up", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("round millis up result", 123024, result);
	}

	{
		const etTime t = { .sec = 123, .nSec = 23456456 };
		ETUNIT_TRUE("round millis down", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("round millis down result", 123023, result);
	}

	{
		const etTime t = { .sec = 123, .nSec = 0 };
		ETUNIT_TRUE("only positive seconds", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("only positive seconds result", 123000, result);
	}
	
	{
		const etTime t = { .sec = -123, .nSec = 0 };
		ETUNIT_TRUE("only negative seconds", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("only negative seconds result", -123000, result);
	}

	{
		const etTime t = { .sec = 0, .nSec = 123000000 };
		ETUNIT_TRUE("only positive nanoseconds", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("only positive nanoseconds result", 123, result);
	}

	{
		const etTime t = { .sec = 0, .nSec = -123000000 };
		ETUNIT_TRUE("only negative nanoseconds", etUnit_timeInMillis32(&t, &result));
		ETUNIT_EQUAL_INT("only negative nanoseconds result", -123, result);
	}
}

void TestEtUnit_runSuite(void){
	etUnit_openTestSuite("org.eclipse.etrice.runtime.c.tests.TestEtUnit");
	// test parallel first due test case number restriction
	TestEtUnit_Parallel_TestCase();
	TestEtUnit_Singleton_TestCase();
	TestEtUnit_Skip_TestCase();
	ADD_TESTCASE(TestEtUnit_Expect_Order);
	ADD_TESTCASE(TestEtUnit_Expect);
	ADD_TESTCASE(TestEtUnit_timeInMillis32);
	etUnit_closeTestSuite();
}


